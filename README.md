## Main usage

```
Usage: main.py [OPTIONS] COMMAND [ARGS]...

  FXscraper is an tool mainly created for scraping subreddits but     there
  are plans on expending it with support for Youtube as well as other known
  media sources.

Options:
  -v, --verbose           Enable verbose mode.      [print output]
  -i, --interactive       Enable interactive mode.  [print download
                          information]
  -d, --downloaddir TEXT  Download directory.       [default:
                          $home/downloads/]
  -lf, --logfile TEXT     Logfile location.         [default: $home/log/FX-
                          scraper]
  -ll, --loglevel TEXT    Log level.                [default: INFO  |  choose
                          from:ERROR, WARNING, INFO, DEBUG, ERROR, CRITICAL]
  --help                  Show this message and exit.

Commands:
  reddit  Use Reddit as mediasource.
```

## Reddit command usage

```
Usage: main.py reddit [OPTIONS] SUBREDDIT

  Use Reddit as mediasource.

Options:
  -d, --depth INTEGER  Number of posts (same as limit). Only working with the
                       hot,  Default_value = 10
  -s, --sort TEXT      Type of posts. Defaut_value = rising. Choose frome:
                       [new, rising, hot, controvertial]
  -p, --period TEXT    The time frame period. Default_value = day
  --help               Show this message and exit.
```

## Example:

```
λ root@helga [~] → python3.6 /opt/fx-scraper/main.py -i reddit -s hot -d 5 funny

                                               _..
                                           .qd$$$$bp.
                                         .q$$$$$$$$$$m.
                                        .$$$$$$$$$$$$$$
                                      .q$$$$$$$$$$$$$$$$
                                     .$$$$$$$$$$$$P\$$$$;
                                   .q$$$$$$$$$P^"_.`;$$$$
                                  q$$$$$$$P;\   ,  /$$$$P
                                .$$$P^::Y$/`  _  .:.$$$/
                               .P.:..    \ `._.-:.. \$P
                               $':.  __.. :   :..    :'
                              /:_..::.   `. .:.    .'|
                            _::..          T:..   /  :
                         .::..             J:..  :  :
                      .::..          7:..   F:.. :  ;
                  _.::..             |:..   J:.. `./
             _..:::..               /J:..    F:.  :
           .::::..                .T  \:..   J:.  /
          /:::...               .' `.  \:..   F_o'
         .:::...              .'     \  \:..  J ;
         ::::...           .-'`.    _.`._\:..  '
         ':::...         .'  `._7.-'_.-  `\:.   \.
          \:::...   _..-'__.._/_.--' ,:.   b:.   \.
           `::::..-"_.'-"_..--"      :..   /):.   `.
             `-:/"-7.--""            _::.-'P::..    \.
  _....------""""""            _..--".-'   \::..     `.
 (::..              _...----"""  _.-'       `---:..    `-.
  \::..      _.-""""   `""""---""                `::...___)
   `\:._.-"""                     __FX-scraper__
   

[  ***  ] [1] 	Downloading post: /r/funny/comments/dhkc86/found_in_central_park_stephen_if_youre_out_there
[  ***  ] [2] 	Downloading post: /r/funny/comments/dhkv38/here_have_a_souvenir_kid
[  ***  ] [3] 	Downloading post: /r/funny/comments/dhio84/this_sign_in_an_antique_store
[  ***  ] [4] 	Downloading post: /r/funny/comments/dhicot/kung_fu_sheep
[  ***  ] [5] 	Downloading post: /r/funny/comments/dhfigp/irish_man_leaves_funny_recording_for_his_funeral

Program finished! Statistics:
        Subreddit             :  funny
        Otions                :  Sort: hot | Period: day | Depth: 5
        Download directory    :  /root/downloads
        Log directory         :  /root/log/FX-scraper.log
        Total posts scraped   :  6
        Unkown posts/urls     :  1
        Download successfully :  2
        Skipped duplicates    :  3
        Runtime               :  0:00:04
        Unkown urls:          :  https://www.reddit.com/r/PinkFloydCircleJerk/
```


TODO:

  - [ ] Add functionality for a configfile
  - [ ] Create docker-file
  - [ ] Create pip package
  - [ ] Implement async
