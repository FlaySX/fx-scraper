try:
    import logging
    import time
    import signal
    import json
    import sys
    import requests
    from gfycat.client import GfycatClient

except ImportError as e:
    exit(f'ImportError: {e}')


def signal_handler(sig, frame):
        print('\nYou pressed Ctrl+C! Exiting now...')
        sys.exit(0)

class Subreddit(object):
    signal.signal(signal.SIGINT, signal_handler)

    def __init__(self, subreddit, depth, sort, period):
        logging.debug(f'Reddit command was activated with the following options: (subreddit={subreddit} depth={depth} sort={sort} period={period})')

        self.subreddit = subreddit
        self.depth = depth
        self.sort = sort
        self.period = period
        self.ready_for_download = list()
        self.ytdl_posts = list()
        self.unknown_posts = list()

        if self.sort not in ['top', 'controversial', 'hot', 'rising', 'new']:
            msg = f"'{self.sort} in unsupported!!"
            logging.error(msg)
            exit(msg +  ' Exiting now..')


    def get_raw_data(self):
        url = f'https://www.reddit.com/r/{self.subreddit}/{self.sort}.json'

        if self.sort in ['top', 'controversial']:
            url = url + f"?t={self.period}"

        else:
            url = url + f"?limit={self.depth}"

        headers = {'user-agent': 'FX_Scraper/0.1'}
        logging.info('Getting RAW data from {} url: {}'.format(self.subreddit, url))

        r = requests.get(url, headers=headers)

        if not r.status_code == 200 and not r.status_code == 404:
            logging.error(f"Unexpected response code: {r.status_code}")
            exit(f'Did not recieve get an 200 OK or 404 when getting raw_posts_data! StatusCode: {r.status-code}')

        elif r.status_code == 404:
            logging.error('Specified subreddit does not exist! 404 not found!')
            exit(f"Error: Subreddit '{self.subreddit}' not found!")


        self.raw_data = r.json().get('data').get('children')

        return self.raw_data



    def get_posts_from_raw_data(self):
        logging.info('Filtering the raw_data..')
        filtered_posts = list()

        for post in self.raw_data:
            try:
                post_data = {
                    'title': post.get('data').get('title').encode('ascii', 'ignore').decode('utf8'),
                    'domain': post.get('data').get('domain').encode('ascii', 'ignore').decode('utf8'),
                    'sub': post.get('data').get('subreddit').encode('ascii', 'ignore').decode('utf8'),
                    'url': post.get('data').get('url').encode('ascii', 'ignore').decode('utf8'),
                    'is_video': post.get('data').get('is_video'),
                    'permalink': post.get('data').get('permalink').encode('ascii', 'ignore').decode('utf8'),
                    'ytdl': False
                }
            except Exception as e:
                logging.error(f"Something went wrong while extracting data from posts \t\t Msg: {e}")

            filtered_posts.append(post_data)

        self.posts = filtered_posts

        return filtered_posts


    def get_download_urls(self):
        accepted_domains = ['i.imgur.com', 'i.redd.it', 'imgur.com']
        accepted_formats = ['jpg', 'mp4', 'jpeg', 'gif', 'png', 'gifv']
        youtubedl_domains = ['youtube.com', 'm.youtube.com', 'youtu.be',\
                             'v.redd.it', \
                             'pornhub.com', 'm.pornhub.com',\
                             'xvideos.com', 'm.xvideos.com' , \
                             'youporn.com', 'm.youporn.com' \
                             'xhamster.com', 'm.xhamster.com' \
                             'vimeo.com', 'm.vimeo.com'\
                             'gifdeliverynetwork.com', 'redgifs.com']

        # Gfycat access and private-key
        client = GfycatClient('2_4Vj9yo', 'MbuoTwzv7ZqD9H4ZWqP-2DPa9Z2XqZtA1PvsxECG36b7PC4sj0GSajiKuG58yyio')

        for post in self.posts:
            normal_url = None
            ytdl_url = None
            data = None

            logging.info(f"Extracting download url from: https://www.reddit.com{post['permalink']}")

            if post['domain'] in ['gfycat.com']:
                postid = post['url'].split('/')[-1]

                try:
                    data = client.query_gfy(f"{postid}")['gfyItem']

                except Exception as e:
                    logging.error(f"Error in gfycat API: {e}")
                    post['url'] = post['url'].replace('gfycat.com', 'gifdeliverynetwork.com')
                    post['ytdl'] = True
                    logging.info(f"Changing domain from gfycat.com to gifdeliverynetwork.com and try to download with youtube-dl: {post['url']}")

                if data:
                    try:
                        normal_url = data['mp4Url']

                    except KeyError as e:
                        logging.warning(f"{logger_msg} KeyError{e}")
                        normal_url = data['url']

            elif post['domain'] == 'i.imgur.com':
                normal_url = post['url'].replace('gifv', 'mp4')

            elif post['domain'] in ['imgur.com', 'm.imgur.com']:
                normal_url = post['url']

            elif post['url'].split('.')[-1] in accepted_formats:
                normal_url = post['url']

            elif post['domain'] in youtubedl_domains:
                normal_url = post['url']
                post['ytdl'] = True

            else:
                self.unknown_posts.append(post)
                logging.warn(f"Unsupported url: {post['url']}")

            if normal_url:
                post['downloadurl'] = normal_url
                self.ready_for_download.append(post)
