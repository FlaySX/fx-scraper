try:
    import logging
    import os
    from pathlib import Path
    import time
    import sys
    import json
    import requests
    from slugify import slugify
    from multiprocessing.pool import ThreadPool
    from imgur_downloader import ImgurDownloader
    import youtube_dl

except ImportError as e:
    exit(f'ImportError: {e}')


def execute_command(msg, cmd, argument):

    if not isinstance(argument, list):
        argument = [argument]

    '''Create pool and execut command'''
    pool = ThreadPool(processes=1)
    res = pool.apply_async(cmd, args=argument)

    '''Print loading animation and get output when proccess is done'''
    while not res.ready():
        chars = ['[   *** ]', '[    ***]', '[   *** ]', '[  ***  ]', '[ ***   ]', '[***    ]', '[ ***   ]', '[  ***  ]']
        #chars = ['|', '/', '-', '\\', '|']

        for char in chars:
            sys.stdout.write('\r{:<12} {}'.format(char, msg))
            time.sleep(.1)
            sys.stdout.flush()

    sys.stdout.flush()
    return res.get()


def init_logger(loglevel, logfile, verbose):

    file = logfile.split('/')[-1]
    path = logfile.rstrip(file).rstrip('/')

    try:
        h = [logging.FileHandler(logfile)]
    except FileNotFoundError:
        logging.warning('Missing logfile! Will create a new one..')

        os.mkdir(path, 0o755)
        Path(file).touch()


    if verbose:
        h.append(logging.StreamHandler())

    try:
        logging.basicConfig(
            level=loglevel,
            format='%(asctime)s {:<15} \t {:<15} \t {} '.format('%(levelname)s', '%(module)s', '%(message)s'),
            datefmt='%a, %d %b %Y %H:%M:%S',
            handlers=h
        )

    except ValueError:
        msg = "Error! Unkown log_level: '{}'. Please choose from: ERROR, WARNING, INFO, DEBUG, NOTSET"
        exit(msg.format(loglevel))

    except PermissionError:
        msg = "Error! PermissionError: Cannot create log_file: '{}'. Make sure you have the right permissions!"
        exit(msg.format(logfile))


def download(post, path, create_dir=True, youtubedl=False):

    domain = post['downloadurl'].split('/')[2]
    filename = ('_'.join(post['title'].split(' ')) + '.' + post['downloadurl'].split('.')[-1])
    filename = filename.replace('/', '_')

    if len(filename) > 150:
        filename = filename[:150]

    if create_dir:
        download_dir = path + '/' + post['sub']

    if not os.path.isdir(download_dir):
        logging.info(f"Download directory '{download_dir}' does not exits. Creating it now..")
        os.makedirs(download_dir)

    if os.path.isfile(download_dir + '/' + filename):
        logging.info(f'Skipped duplicate: {download_dir}/{filename}')

        return 'Duplicate'


    # Download igmur posts with the igmur-downloader module
    if post['domain'] == 'imgur.com':
        file = ImgurDownloader(post['url'])
        file.save_images(download_dir + '/')

        return 'Finished'

    elif not post['ytdl']:
        req = requests.get(post['downloadurl'])

        if not req.status_code == 200:
            logging.error(f"Error: Response code is not 200! {req.status_code} url: {post['downloadurl']}")

        else:
            logging.info(f"Downloading Post:[{post['permalink']}] From:[{post['downloadurl']}] Filename:[{filename}]")

            with open(download_dir + '/' + filename, 'wb') as f:
                for chunk in req.iter_content(1024):
                    f.write(chunk)
                return 'Finished'

    # Download from youtube and other sources with youtube-dl module
    elif post['ytdl']:
        logging.info(f"Downloading post with youtubedl:[{post['permalink']}] From:[{post['url']}]")

        ydl_opts = {
            'outtmpl': f"{download_dir}/%(title)s.%(ext)s",
            'quiet': True,
            'no_warnings': True,
            'retries': 5

        }

        try:
            with youtube_dl.YoutubeDL(ydl_opts) as ydl:
                ydl.download([post['url']])

                return 'Finished'

        except Exception as e:
            logging.error(f"Something went wrong while downloding {post['url']}, Error: {e}")
            return 'Failed'
