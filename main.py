#!/usr/bin/env python

try:
    import click
    import datetime
    import logging
    import os
    import sys
    import signal
    import time
    from pathlib import Path
    from utils import init_logger
    from utils import download
    from utils import execute_command
    from libs.subreddit import Subreddit


except ImportError as e:
    exit(f'ImportError: {e}')


banner = '''
                                               _..
                                           .qd$$$$bp.
                                         .q$$$$$$$$$$m.
                                        .$$$$$$$$$$$$$$
                                      .q$$$$$$$$$$$$$$$$
                                     .$$$$$$$$$$$$P\$$$$;
                                   .q$$$$$$$$$P^"_.`;$$$$
                                  q$$$$$$$P;\   ,  /$$$$P
                                .$$$P^::Y$/`  _  .:.$$$/
                               .P.:..    \ `._.-:.. \$P
                               $':.  __.. :   :..    :'
                              /:_..::.   `. .:.    .'|
                            _::..          T:..   /  :
                         .::..             J:..  :  :
                      .::..          7:..   F:.. :  ;
                  _.::..             |:..   J:.. `./
             _..:::..               /J:..    F:.  :
           .::::..                .T  \:..   J:.  /
          /:::...               .' `.  \:..   F_o'
         .:::...              .'     \  \:..  J ;
         ::::...           .-'`.    _.`._\:..  \'
         ':::...         .'  `._7.-'_.-  `\:.   \.
          \:::...   _..-'__.._/_.--' ,:.   b:.   \.
           `::::..-"_.'-"_..--"      :..   /):.   `.
             `-:/"-7.--""            _::.-'P::..    \.
  _....------""""""            _..--".-'   \::..     `.
 (::..              _...----"""  _.-'       `---:..    `-.
  \::..      _.-""""   `""""---""                `::...___)
   `\:._.-"""                     __FX-scraper__
   
'''


def signal_handler(sig, frame):
        print('\nYou pressed Ctrl+C! Exiting now...')
        sys.exit(0)
@click.group(invoke_without_command=True, help=('FXscraper is an tool mainly created for scraping subreddits but \
    there are plans on expending it with support for Youtube as well as other known media sources.'))
@click.option('--verbose',      '-v',  help='Enable verbose mode.      [print output]',       is_flag=True, default=False,)
@click.option('--interactive',  '-i',  help='Enable interactive mode.  [print download information]',   is_flag=True, default=False)
@click.option('--downloaddir',  '-d',  help='Download directory.       [default: $home/downloads/]', default=f"{Path.home()}/downloads")
@click.option('--logfile',      '-lf', help='Logfile location.         [default: $home/log/FX-scraper]', default=f"{Path.home()}/log/fx-scraper/main.log")
@click.option('--loglevel',     '-ll', help='Log level.                [default: INFO  |  choose from:ERROR, WARNING, INFO, DEBUG, ERROR, CRITICAL]', default='INFO')
@click.pass_context
def cli(ctx, verbose, interactive, downloaddir, logfile, loglevel):
    if ctx.invoked_subcommand is None:
        print(banner)
        
        click.echo(ctx.get_help())
    
    ctx.obj = dict()
    ctx.obj['verbose'] = verbose
    ctx.obj['interactive'] = interactive
    ctx.obj['downloaddir'] = downloaddir
    ctx.obj['start_time'] = time.time()
    ctx.obj['logfile'] = logfile

    if interactive:
        b = banner.split('\n')
        for i in b:
            print(i)
            time.sleep(0.03)

    # Initiatin program logging
    init_logger(loglevel, logfile, verbose)
    logging.debug(f"Program started! time: {ctx.obj['start_time']}")
    if verbose and interactive:
        logging.warn('Both verbose and interactive modes and not supported because of fucked up output. Disabeling interactive mode now..')
        ctx.obj['interactive'] = False


@cli.command(help='Use Reddit as mediasource.')
@click.option('--depth',  '-d', help='Number of posts (same as limit). Only working with the hot,  Default_value = 10', default=10)
@click.option('--sort',   '-s', help='Type of posts. Defaut_value = rising. Choose frome: [new, rising, hot, controvertial]', default='rising')
@click.option('--period', '-p', help='The time frame period. Default_value = day', default='day')
@click.argument('subreddit')
@click.pass_context
def reddit(ctx, depth, subreddit, sort, period):
    interactive = ctx.obj['interactive']
    downloaddir = ctx.obj['downloaddir']
    start_time = ctx.obj['start_time']
    logfile = ctx.obj['logfile']

    subdata = Subreddit(subreddit, depth, sort, period)
    subdata.get_raw_data()
    subdata.get_posts_from_raw_data()
    subdata.get_download_urls()
    
    finished     = 0 
    skipped      = 0
    failed       = 0
    post_counter = 1

    for post in subdata.ready_for_download:
        try:
            if interactive:
                msg = "[{}]\t{}".format(post_counter, post['permalink'].rstrip('/')).expandtabs(3)
                res = execute_command(msg, download, [post, downloaddir])

            else:
                res = download(post, downloaddir)
        except Exception as e:
            res = 'Failed'
            logging.debug('Error has occured!')
            logging.error(f'\tMSG: {e}')

        finished = finished + 1 if res == 'Finished'  else finished
        skipped  = skipped  + 1 if res == 'Duplicate' else skipped
        failed   = failed   + 1 if res == 'Failed'    else failed
        post_counter = post_counter + 1

        if interactive:
            res = "[" + res + "]"
            sys.stdout.write("\r{:<13}".format(res) + msg)
            print()

    runtime = int(time.time() - start_time)
    runtime = str(datetime.timedelta(seconds=runtime))
    
    stats = f'''\nProgram finished! Statistics:
        Subreddit             :  {subreddit}
        Otions                :  Sort: {sort} | Period: {period} | Depth: {depth}
        Download directory    :  {downloaddir}
        Log directory         :  {logfile}
        Total posts scraped   :  {(len(subdata.unknown_posts) + finished + skipped)}
        Unkown posts/urls     :  {len(subdata.unknown_posts)}
        Download successfully :  {finished}
        Failed posts          :  {failed} 
        Skipped duplicates    :  {skipped}
        Runtime               :  {runtime}
        Unkown urls:          :  {', '.join([post['url'] for post in subdata.unknown_posts])}'''

    logging.info(stats)
    print(stats)

    logging.debug('Program ended')


if __name__ == '__main__':
    signal.signal(signal.SIGINT, signal_handler)
    cli()



